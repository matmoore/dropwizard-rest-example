Example Java REST web service with DropWizard
=============================================

Implemented
-----------

* Maven build with fat jar
* A few odds conversion tests
* Basic health check

**GET /available**

* Returns a list of bets
* Integrates with upstream API
* Converts odds from fractional to decimal format

**POST /bets/**

* Performs basic input validation
* Integrates with upstream API
* Converts odds between fractional and decimal

Possible improvements
---------------------
* Validation of fractional odds (we only check decimal values)
* Unit tests for API responses (this would require mocks of the Sky API)