package uk.co.MatMoore.moorebis.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Internal Odds representation with conversion from fractional to decimal.
 *
 * Note that this does not attempt to simplify the odds into something
 * user friendly.
 *
 * Customers expect odds to be presented with a small denominator
 * and in practice certain odds will be very common.
 * The upstream service doesn't seem to do this so we have to simplify here.
 */
public class Odds {
    private static final Logger log = LoggerFactory.getLogger(Odds.class.getName());
    private int num;
    private int den;

    /**
     * Euclid's algorithm for greatest common divisor
     * When in doubt, outsource to the greeks
     */
    public static int GCD(int a, int b) {
        if (b == 0) {
            return a;
        }
        return GCD(b, a % b);
    }

    /**
     * Fractional odds
     * Return = (1 + num / den) * stake
     *
     * TODO: constrain invalid input
     *
     * @param num Positive integer
     * @param den Positive integer
     */
    public Odds(int num, int den) {
        int gcd = GCD(num, den);
        log.info(String.format("Simplifying %d-%d with gcd %d", num, den, gcd));
        this.num = num / gcd;
        this.den = den / gcd;
    }

    /**
     * Alternate constructor for fractional odds.
     * Return = decimal * stake
     * @param decimal
     */
    public Odds(BigDecimal decimal) {
        // Example: With 11.0 the return is 11 times the stake, or 10 for every 1 staked.
        // We return as 1000/100 rather than 10/1.
        this(decimal.subtract(BigDecimal.ONE).scaleByPowerOfTen(2).intValue(), 100);
    }

    public String toString() {
        return String.format("%d-%d", num, den);
    }

    /**
     * TODO: remove unchecked ArithmeticException
     * @return The decimal version of the odds, using half down rounding.
     */
    public BigDecimal toDecimal() {
        return new BigDecimal(this.num)
                .setScale(2)
                .divide(new BigDecimal(this.den), RoundingMode.HALF_DOWN)
                .add(BigDecimal.ONE);
    }

    public int getNum() {
        return num;
    }

    public int getDen() {
        return den;
    }
}
