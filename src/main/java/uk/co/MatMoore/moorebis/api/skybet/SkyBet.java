package uk.co.MatMoore.moorebis.api.skybet;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;


/**
 * A bet.
 */
public class SkyBet {
    @NotNull
    private long id;

    private Map<String, Integer> fractionalOdds;

    @DecimalMin("0.01")
    @NotNull
    private BigDecimal stake;

    public SkyBet() {
        // Jackson deserialization
    }

    /**
     * @param id The bet_id in Skybet's API. This identifies the selection.
     * @param fractionalOdds The numerator and denominator for the odds the bet should be placed at.
     * @param stake The total stake to be placed on the bet.
     */
    public SkyBet(@JsonProperty("bet_id") long id, @JsonProperty("odds") Map<String, Integer> fractionalOdds, @JsonProperty("stake") BigDecimal stake) {
        this.id = id;
        this.fractionalOdds = fractionalOdds;
        this.stake = stake;
    }

    @JsonProperty("bet_id")
    public long getBetId() {
        return id;
    }

    public BigDecimal getStake() {
        return this.stake;
    }

    public Map<String, Integer> getOdds() {
        return this.fractionalOdds;
    }
}
