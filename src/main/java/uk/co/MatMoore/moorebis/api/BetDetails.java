package uk.co.MatMoore.moorebis.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;


/**
 * Details about a placed bet.
 */
public class BetDetails {
    private long id;
    private BigDecimal decimalOdds;
    private BigDecimal stake;
    private String event;
    private String name;
    private long transactionId;

    public BetDetails() {
        // Jackson deserialization
    }

    /**
     * @param id The bet_id in Skybet's API. This identifies the selection.
     * @param decimalOdds The odds the bet should be placed at.
     * @param stake The total stake to be placed on the bet.
     * @param eventName The name of the event
     * @param selectionName The name of the selection being bet on
     * @param transactionId The transaction_id in Skybet's API. This identifies the placed bet.
     */
    public BetDetails(long id, BigDecimal decimalOdds, BigDecimal stake, String eventName, String selectionName, long transactionId) {
        this.id = id;
        this.decimalOdds = decimalOdds;
        this.stake = stake;
        this.event = eventName;
        this.name = selectionName;
        this.transactionId = transactionId;
    }

    @JsonProperty("bet_id")
    public long getBetId() {
        return id;
    }

    @JsonProperty("transaction_id")
    public long getTransactionId() {
        return transactionId;
    }

    @JsonProperty
    public BigDecimal getStake() {
        return this.stake;
    }

    @JsonProperty
    public String getEvent() {
        return this.event;
    }

    @JsonProperty
    public String getName() {
        return this.name;
    }

    @JsonProperty("odds")
    public BigDecimal getDecimalOdds() {
        return this.decimalOdds;
    }
}
