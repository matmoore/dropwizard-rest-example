package uk.co.MatMoore.moorebis.api.skybet;

import uk.co.MatMoore.moorebis.util.Odds;
import java.util.HashMap;
import java.util.Map;

public class SkyUtil {
    final static String NUM = "numerator";
    final static String DEN = "denominator";

    /**
     * Represent odds as a map for easy JSON serialization
     * @param odds
     * @return Map with SkyUtil.NUM and SkyUtil.DEN keys
     */
    public static Map<String, Integer> buildOdds(Odds odds) {
        Map<String, Integer> oddsMap = new HashMap<String, Integer>();
        oddsMap.put(NUM, odds.getNum());
        oddsMap.put(DEN, odds.getDen());
        return oddsMap;
    }
}
