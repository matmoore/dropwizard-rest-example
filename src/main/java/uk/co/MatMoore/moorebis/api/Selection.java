package uk.co.MatMoore.moorebis.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;


/**
 * Something that can be bet on
 */
public class Selection {
    private long id;
    private String eventName;
    private String selectionName;
    private BigDecimal decimalOdds;

    public Selection() {
        // Jackson deserialization
    }

    public Selection(long id, String eventName, String selectionName, BigDecimal decimalOdds) {
        this.id = id;
        this.eventName = eventName;
        this.selectionName = selectionName;
        this.decimalOdds = decimalOdds;
    }

    @JsonProperty("bet_id")
    public long getBetId() {
        return id;
    }

    @JsonProperty("event")
    public String getEventName() {
        return this.eventName;
    }

    @JsonProperty("name")
    public String getName() {
        return this.selectionName;
    }

    @JsonProperty("odds")
    public BigDecimal getDecimalOdds() {
        return this.decimalOdds;
    }
}
