package uk.co.MatMoore.moorebis.api.skybet;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.MatMoore.moorebis.api.BetDetails;
import uk.co.MatMoore.moorebis.util.Odds;

import java.math.BigDecimal;
import java.util.Map;


/**
 * Details about a placed bet.
 */
public class SkyBetDetails {
    private long id;
    private Map<String, Integer> fractionalOdds;
    private BigDecimal stake;
    private String event;
    private String name;
    private long transactionId;

    public SkyBetDetails() {
        // Jackson deserialization
    }

    /**
     * @param id The bet_id in Skybet's API. This identifies the selection.
     * @param fractionalOdds The odds the bet should be placed at.
     * @param stake The total stake to be placed on the bet.
     * @param eventName The name of the event
     * @param selectionName The name of the selection being bet on
     * @param transactionId The transaction_id in Skybet's API. This identifies the placed bet.
     */
    public SkyBetDetails(@JsonProperty("bet_id") long id, @JsonProperty("odds") Map<String, Integer> fractionalOdds, @JsonProperty("stake") BigDecimal stake, @JsonProperty("event") String eventName, @JsonProperty("name") String selectionName, @JsonProperty("transaction_id") long transactionId) {
        this.id = id;
        this.fractionalOdds = fractionalOdds;
        this.stake = stake;
        this.event = eventName;
        this.name = selectionName;
        this.transactionId = transactionId;
    }

    @JsonProperty("bet_id")
    public long getBetId() {
        return id;
    }

    @JsonProperty("transaction_id")
    public long getTransactionId() {
        return transactionId;
    }

    @JsonProperty
    public BigDecimal getStake() {
        return this.stake;
    }

    @JsonProperty
    public String getEvent() {
        return this.event;
    }

    @JsonProperty
    public String getName() {
        return this.name;
    }

    @JsonProperty("odds")
    public Map<String, Integer> getFractionalOdds() {
        return this.fractionalOdds;
    }

    public Odds getOdds() {
        return new Odds(fractionalOdds.get(SkyUtil.NUM), fractionalOdds.get(SkyUtil.DEN));
    }

    public BetDetails toBetDetails() {
        return new BetDetails(getBetId(), getOdds().toDecimal(), getStake(), getEvent(), getName(), getTransactionId());
    }
}
