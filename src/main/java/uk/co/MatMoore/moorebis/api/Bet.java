package uk.co.MatMoore.moorebis.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.MatMoore.moorebis.api.skybet.SkyBet;
import uk.co.MatMoore.moorebis.api.skybet.SkyUtil;
import uk.co.MatMoore.moorebis.resources.BetResource;
import uk.co.MatMoore.moorebis.util.Odds;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A bet.
 */
public class Bet {
    private static final Logger log = LoggerFactory.getLogger(BetResource.class.getName());

    @NotNull
    private long id;

    @DecimalMin("1")
    @NotNull
    private BigDecimal decimalOdds;

    @DecimalMin("0.01")
    @NotNull
    private BigDecimal stake;

    public Bet() {
        // Jackson deserialization
    }

    /**
     * @param id The bet_id in Skybet's API. This identifies the selection.
     * @param decimalOdds The odds the bet should be placed at.
     * @param stake The total stake to be placed on the bet.
     */
    public Bet(@JsonProperty("bet_id") long id, @JsonProperty("odds") BigDecimal decimalOdds, @JsonProperty("stake") BigDecimal stake) {
        this.id = id;
        this.decimalOdds = decimalOdds;
        this.stake = stake;
    }

    public long getBetId() {
        return id;
    }

    public BigDecimal getStake() {
        return this.stake;
    }

    public Odds getOdds() {
        Odds odds = new Odds(this.decimalOdds);
        log.info(String.format("Converted odds %.2f -> %s", this.decimalOdds, odds));
        return odds;
    }

    public SkyBet toSkyBet() {
        return new SkyBet(getBetId(), SkyUtil.buildOdds(getOdds()), getStake());
    }
}
