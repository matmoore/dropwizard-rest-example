package uk.co.MatMoore.moorebis;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class LocalConfiguration extends Configuration {
    @NotEmpty
    private String skybetAPI;

    @JsonProperty
    public String getSkybetAPI() {
        return skybetAPI;
    }

    @JsonProperty
    public void setSkybetAPI(String skybetAPI) {
        this.skybetAPI = skybetAPI;
    }
}
