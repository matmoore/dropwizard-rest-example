package uk.co.MatMoore.moorebis.resources;

import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.MatMoore.moorebis.api.Bet;
import uk.co.MatMoore.moorebis.api.BetDetails;
import uk.co.MatMoore.moorebis.api.skybet.SkyBet;
import uk.co.MatMoore.moorebis.api.skybet.SkyBetDetails;
import uk.co.MatMoore.moorebis.health.UpstreamHealthCheck;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;


@Path("/bets")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BetResource {
    private static final Logger log = LoggerFactory.getLogger(BetResource.class.getName());
    private WebTarget endpoint;
    private UpstreamHealthCheck healthCheck;

    public BetResource(WebTarget endpoint, UpstreamHealthCheck healthCheck) {
        this.endpoint = endpoint;
        this.healthCheck = healthCheck;
    }

    /**
     * Proxies the bet request to the Skybet API
     * In case of error, the JSON error message and status code will be returned
     * to the client without modification.
     *
     * Common error cases include:
     * - the bet ID does not match to an active selection
     * - stake too high or low
     * - the price has changed
     *
     * @param bet A valid Bet object
     * @return
     */
    @POST
    @Timed
    public BetDetails create(@Valid Bet bet) {
        SkyBet skybet = bet.toSkyBet();
        try {
            SkyBetDetails response = this.endpoint.request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(skybet), SkyBetDetails.class);
            return response.toBetDetails();
        } catch (WebApplicationException e) {
            healthCheck.registerLastResult(e.getResponse().getStatus());
            throw e; // We just proxy error responses through to the client
        }
    }
}
