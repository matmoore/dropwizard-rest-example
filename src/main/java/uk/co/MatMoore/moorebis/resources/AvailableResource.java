package uk.co.MatMoore.moorebis.resources;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.MatMoore.moorebis.api.Selection;
import uk.co.MatMoore.moorebis.health.UpstreamHealthCheck;
import uk.co.MatMoore.moorebis.util.Odds;
import com.codahale.metrics.annotation.Timed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("/available")
@Produces(MediaType.APPLICATION_JSON)
public class AvailableResource {
    private static final Logger log = LoggerFactory.getLogger(AvailableResource.class.getName());
    private WebTarget endpoint;
    private UpstreamHealthCheck healthCheck;

    public AvailableResource(WebTarget endpoint, UpstreamHealthCheck healthCheck) {
        this.endpoint = endpoint;
        this.healthCheck = healthCheck;
    }

    @GET
    @Timed
    public List<Selection> list() throws IOException {
        Response response = this.endpoint.request().accept("application/json").get();
        int status = response.getStatus();

        ArrayList<Selection> results = new ArrayList<Selection>();

        String json = response.readEntity(String.class);
        healthCheck.registerLastResult(status);
        log.info(String.format("Upstream returned: %d", status));
        if (status != 200) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        // This is long winded but I'm not familiar enough with jersey to do the mapping properly
        ObjectMapper mapper = new ObjectMapper();

        try {
            JsonNode root = mapper.readTree(json);
            for (JsonNode item : root) {
                long bet_id = item.get("bet_id").asLong();
                String event = item.get("event").asText();
                String name = item.get("name").asText();
                JsonNode odds = item.get("odds");
                int num = odds.get("numerator").asInt();
                int den = odds.get("denominator").asInt();
                results.add(new Selection(bet_id, event, name, (new Odds(num, den)).toDecimal()));
            }

        } catch (IOException e) {
            log.error("Failed to parse JSON for available bets");

            // This just assumes it was the client's fault
            throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
        }

        return results;
    }
}
