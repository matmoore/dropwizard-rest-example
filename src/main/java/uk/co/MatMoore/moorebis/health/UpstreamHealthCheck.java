package uk.co.MatMoore.moorebis.health;
import com.codahale.metrics.health.HealthCheck;

import java.net.URI;
import java.util.Optional;

/**
 * Check that we can access the Skybet API
 */
public class UpstreamHealthCheck extends HealthCheck {
    private final Optional<URI> uri;
    private boolean lastResponseFailed = false;

    /**
     * If we're not provided with a parsed URI, this health check will stay
     * in a failed state. Probably this check can be done more simply in the
     * configuration class, but I'm unfamiliar with DropWizard, so it's just
     * shoehorned in here.
     */
    public UpstreamHealthCheck(Optional<URI> uri) {
        this.uri = uri;
    }

    /**
     * Consider the upstream as unhealthy if its last request returned a server error.
     * @param statusCode HTTP status code
     */
    public void registerLastResult(int statusCode) {
        this.lastResponseFailed = (statusCode >= 500);
    }

    @Override
    protected Result check() throws Exception {
        if (!uri.isPresent() || lastResponseFailed) {
            return Result.unhealthy("Malformed URI");
        }
        return Result.healthy();
    }
}
