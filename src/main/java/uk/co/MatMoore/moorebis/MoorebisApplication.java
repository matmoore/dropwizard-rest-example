package uk.co.MatMoore.moorebis;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import org.slf4j.LoggerFactory;
import uk.co.MatMoore.moorebis.health.UpstreamHealthCheck;
import uk.co.MatMoore.moorebis.resources.AvailableResource;
import java.net.URISyntaxException;
import java.util.Optional;
import java.net.URI;
import org.slf4j.Logger;
import uk.co.MatMoore.moorebis.resources.BetResource;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class MoorebisApplication extends Application<LocalConfiguration> {
    private static final Logger log = LoggerFactory.getLogger(MoorebisApplication.class.getName());

    public static void main(String[] args) throws Exception {
        new MoorebisApplication().run(args);
    }

    @Override
    public String getName() {
        return "Moorebis";
    }

    @Override
    public void run(LocalConfiguration configuration,
                    Environment environment) {
        final URI uri;
        String uriString = configuration.getSkybetAPI();
        UpstreamHealthCheck healthCheck;

        // URI comes from the config as a string and may still be invalid
        try {
            uri = new URI(uriString);
        } catch (URISyntaxException e) {
            log.error("Invalid API URL: " + uriString, e);

            healthCheck = new UpstreamHealthCheck(Optional.<URI>empty());
            environment.healthChecks().register("skybet", healthCheck);
            return;
        }


        // TODO: This should be configured with an explicit timeout
        Client client = ClientBuilder.newClient();

        WebTarget endpoint = client.target(uri);
        WebTarget betEndpoint = endpoint.path("bets");
        WebTarget availableEndpoint = endpoint.path("available");

        healthCheck = new UpstreamHealthCheck(Optional.of(uri));
        environment.jersey().register(new AvailableResource(availableEndpoint, healthCheck));
        environment.jersey().register(new BetResource(betEndpoint, healthCheck));

        environment.healthChecks().register("skybet", healthCheck);
    }
}