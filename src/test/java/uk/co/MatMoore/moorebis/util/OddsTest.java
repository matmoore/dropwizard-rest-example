package uk.co.MatMoore.moorebis.util;

import java.math.BigDecimal;
import static org.junit.Assert.*;


public class OddsTest {

    @org.junit.Test
    public void testFractionalToDecimal() throws Exception {
        Odds odds = new Odds(1, 1);
        assertEquals(odds.toDecimal(), new BigDecimal("2.00"));
    }

    @org.junit.Test
    public void testDecimalToFractional() throws Exception {
        Odds odds = new Odds(new BigDecimal(2));
        assertEquals(odds.getNum(), 1);
        assertEquals(odds.getDen(), 1);
    }

    @org.junit.Test
    public void testDecimalPartPreserved() throws Exception {
        Odds odds = new Odds(new BigDecimal(2.5));
        assertEquals(odds.getNum(), 3);
        assertEquals(odds.getDen(), 2);
    }

    @org.junit.Test
    public void testExactDecimal() throws Exception {
        Odds odds = new Odds(1, 10);
        assertEquals(odds.toDecimal(), new BigDecimal("1.10"));
    }

    /**
     * The decimal conversion rounds up/down if necessary
     */
    @org.junit.Test
    public void testInexactDecimal() throws Exception {
        Odds odds = new Odds(1, 3);
        assertEquals(odds.toDecimal(), new BigDecimal("1.33"));
    }
}